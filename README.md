# what's this

This repo is the source for the openshift container plattform (OKD, openshift origin) router with haproxy 2.2.

## docker link

https://hub.docker.com/r/me2digital/okd-router-hap22

# run this image

## check the current image
```
oc -n default get dc/router \
  -o jsonpath='{.spec.template.spec.containers[0].image}{"\n"}'
```

## Patch the new image
```
oc -n default patch dc/router \
  --type='json' \
  -p='[{"op": "replace", "path": "/spec/template/spec/containers/0/image","value":"docker.io/me2digital/okd-router-hap22:latest"}]'
```

After the patch will the router be deployed.

You can now enable the haproxy-enhancements as described in the doc.
https://docs.openshift.com/container-platform/3.11/release_notes/ocp_3_11_release_notes.html#ocp-311-haproxy-enhancements

The backend communication (server line) have "alpn h2,http/1.1" therefore will haproxy try to talk with the backend with HTTP/2 
when the backend offers this protocol.

## use cookie attribute SameSite=Strict

```
oc -n <PROJECT/NS> annotate route <YOUR-ROUTE> haproxy.router.openshift.io/cookie_same_site=true
```

# how to build

## Docker build
```
git clone https://gitlab.com/aleks001/okd-router-hap22.git
cd okd-router-hap22.git
docker build --rm --tag me2digital/okd-router-hap22:latest .
```

## on openshift
```
oc new-app https://gitlab.com/aleks001/okd-router-hap22.git
oc logs -f bc/okd-router-hap22
```

wait until the Build is successfully.

```
oc  -n default logs -f router-3-5prbb
I0411 03:58:00.766102       1 template.go:297] Starting template router (v3.11.0+39132cb-398)
I0411 03:58:00.768882       1 metrics.go:147] Router health and metrics port listening at 0.0.0.0:1936 on HTTP and HTTPS
E0411 03:58:00.775065       1 haproxy.go:392] can't scrape HAProxy: dial unix /var/lib/haproxy/run/haproxy.sock: connect: no such file or directory
I0411 03:58:00.803313       1 router.go:481] Router reloaded:
 - Checking http://localhost:80 ...
 - Health check ok : 0 retry attempt(s).
I0411 03:58:00.803395       1 router.go:252] Router is including routes in all namespaces
I0411 03:58:01.053602       1 router.go:481] Router reloaded:
 - Checking http://localhost:80 ...
 - Health check ok : 0 retry attempt(s).
I0411 03:58:06.045254       1 router.go:481] Router reloaded:
 - Checking http://localhost:80 ...
 - Health check ok : 0 retry attempt(s).
I0411 03:58:11.517210       1 router.go:481] Router reloaded:
 - Checking http://localhost:80 ...
 - Health check ok : 0 retry attempt(s).

```

